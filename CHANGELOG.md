# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.1.0/)
and this project adheres to
[Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2023-12-07

### Added

- Add documentation comments.

### Changed

- Update to Godot Engine 4.2.
- Make error messages be pushed to the debugger output instead of being
returned.

## [1.1.4] - 2020-05-24

### Fixed

- Fix crash when passing a JSON string that ends with a comma, which is allowed
by Godot.
- Fix indentation error when using space indentation with more than 1 space.

## [1.1.3] - 2020-01-12

### Changed

- Trivial internal changes.

## [1.1.2] - 2019-08-28

### Changed

- Trivial internal changes.

## [1.1.1] - 2019-03-17

### Changed

- Trivial internal changes.

## [1.1.0] - 2019-03-13

### Added

- New `uglify_json()` method, if necessary to undo formatting in a JSON string.

### Changed

- No longer extend from Node, making it a resource that doesn't need to be
pre-loaded.
- Make use of Godot 3.1's new features:
  - Typing system.
  - Script class naming (making preloading the script no longer needed, just
directly use `JSONBeautifier` instead).

## 1.0.5 - 2018-05-10

### Fixed

- Fix insertion/removal of spaces/tabs inside strings.

## (1.0.2 - 1.0.4) - (2018-03-03 - 2018-05-03)

### Changed

- Trivial internal changes.

## 1.0.1 - 2018-02-03

### Fixed

- Fix non-removal of pre-existing formatting.

[2.0.0]: https://codeberg.org/Yeldham/json-beautifier-for-godot/compare/v1.1.4...v2.0.0
[1.1.4]: https://codeberg.org/Yeldham/json-beautifier-for-godot/compare/v1.1.3...v1.1.4
[1.1.3]: https://codeberg.org/Yeldham/json-beautifier-for-godot/compare/v1.1.2...v1.1.3
[1.1.2]: https://codeberg.org/Yeldham/json-beautifier-for-godot/compare/v1.1.1...v1.1.2
[1.1.1]: https://codeberg.org/Yeldham/json-beautifier-for-godot/compare/v1.1.0...v1.1.1
[1.1.0]: https://codeberg.org/Yeldham/json-beautifier-for-godot/compare/v1.0.0...v1.1.0
