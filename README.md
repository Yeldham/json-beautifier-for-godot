# JSON Beautifier for the Godot Engine

This is the git repository for the asset. For an easier integration with your
project, it's recommended to download it via its
[Asset Library page](https://godotengine.org/asset-library/asset/157).

## License

This asset is licensed under the
[MPL-2.0](https://www.mozilla.org/en-US/MPL/2.0/).
